# Stears Coding Homework

Thanks for your application to Stears

This is a coding homework test for Stears’ Senior Frontend Engineer role. We expect that you will use this test to demonstrate your abilities. Your solution does not need to be perfect, but keep in mind we are hiring for someone who is capable of improving and maintain our current web application UIs, and your solution will be used to judge how well you can do so.

For this role, we prefer software engineer candidates with:
- Experience building frontends with ReactJS, Typescript and NextJS
- Experience deploying to serverless and hybrid (static + dynamic) deployment targets

For each task, you will be given a back story and a couple of tasks to summarise what needs to be done with a strict definition of done. Either complete all of them or attempt as much as possible so we have enough information to assess your skill & experience.

#### Submissions

- Create a private [gitlab](https://gitlab.com/) repository and add @foluso_ogunlana & @chukwudi_umeilechukwu as maintainers
- Create a README.md with the following:
  - Clear set up instructions (assume no knowledge of the submission or the stack)
  - Notes of any new useful tools / patterns you discovered while completing the test

#### Deadlines

You will be given at least 5 working days to complete the task and your deadline will be communicated via email.

#### Assessment

Our favourite submissions are -

1. **Clean & simple code** - Minimal, high quality and well tested code that gets to the heart of the problem and stops there.
2. **Rigorous** - Complete solutions with well researched applications of the right technology choices.
3. **UX & DevX** - Smooth user / developer experience which is easy to deploy, well documented and easy to contribute to.

Your submission will also impress us by demonstrating one or more of the following -

- **Mastery** in your general use of language, libraries, frameworks, architecture / design / deployment patterns
- **Unique skills** in specific areas - e.g. testing

#### Conduct

By submitting, you assure us that you have not shared the test with anyone else and that all work submitted is your own. Though you are allowed to use whatever online or offline resources (including code packages) that you see fit to learn skills for the tasks.

# **Coding Homework**

**Guidelines**
- Complete all tasks if possible
- Include a README.md telling us how to use your code
- FE - Use ReactJS and NextJS. We also like TailwindCSS but it's not a requirement

## Background

You've just joined the Lagos Money Media Company, or LMM.co, and you've been tasked with developing their first web application. You're convinced the team needs to use ReactJS, but the boss wants you to use vanilla HTML and CSS because she doesn't know much about ReactJS or why it's more powerful for Javascript heavy web applications.

### Task 1

You're tasked with developing a component that will be re-used across several sites owned by LMM.co. This component is called the "Article Block". It's displayed in grids, but has not yet got a responsive design. Your boss has asked you to build it in HTML and CSS, but since you know it will be reused, it's also an opportunity to build the component in ReactJS. Can you do both?

Build two versions of the article block grid by eye-balling the picture below - one in HTML and CSS, and one in React. Both versions should work in a grid of 2-4 articles depending on the size of the page, and should use a similar font to the one in the picture, but one that is also freely available.

<img src="article-block.png" alt="article-block" width="500"/>

In case you want to use the exact same images on the picture above, here they are: [first](./task1.1.jpeg), and [second](./task1.2.jpeg)

**Definition of done**:

- [ ] HTML & CSS only article blocks created in a grid of 2-4 depending on the screen size
- [ ] ReactJS article blocks created in a grid of 2-4 depending on the screen size
- [ ] Article block matches the design on the image as closely as possible
- [ ] Clicking either the image or the main text of the article block opens a link to the article
- [ ] Clear documentation or README.md provided, explaining how to set up and run the code
- [ ] Code should also be runnable using docker compose up
- [ ] Unit and/or integration test suite must be available and must execute by running `npm test` or `yarn test`

You can use the example JSON data below:
```json
{
  "count": 2,
  "hasMore": false,
  "items": [
    {
      "slug": "buharis-scorecard-any-hits-or-all-misses",
      "srcSet": "...",
      "title": "Buhari’s scorecard: Any hits or all misses?",
      "summary": "Nigeria's president has struggled with implementing many of his creative and solution-oriented promises.",
      "author": "The Newsroom",
      "pubDate": "2022-07-12T20:00:00.000Z"
    },
    {
      "slug": "how-does-lagos-state-spend-its-money",
      "srcSet": "...",
      "title": "How does Lagos state spend its money?",
      "summary": "Lagos is a megacity in Nigeria but by global standards, it’s not.",
      "author": "Adesola Afolabi",
      "pubDate": "2022-07-14T18:30:00.000Z"
    }
  ]
}
```

### Task 2

After comparing the two components side-by-side, your boss is convinced that ReactJS is the right way to go full steam ahead, and he wants you to use NextJS because of its advanced image optimizations. You've been tasked with creating a page that displays a grid of 8-10 articles in the same manner as in the previous task and to protect the articles with a sign in form so that only allowed users can sign in.

Put your article grid on a NextJS page, and create a sign in / sign up flow using [Next-Auth](https://next-auth.js.org/) to protect the page. You must match the designs in this [figma wireframe](https://www.figma.com/file/InA9Qqc4LKyJg8lYLTahzi/Front-End-Engineer-Interview?node-id=0%3A1), and your designs must respond intelligently to the screen size.

Hints:
- For your convenience, an image is attached, but please make sure to inspect the figma link to get exact measurements
- There might be issues with the design - Missing or incorrect logos, or spaces. Feel free to correct them
- For username and password sign in, you can use a JSON file or custom state management to mock an API retrieving and creating new users (email and password combinations) e.g. see the below

<img src="signin.png" alt="signin" width="800"/>

**Definition of done**:

- [ ] A user is able to sign in with Google or Facebook to see the article grid
- [ ] A user who has signed up, is able to successfully sign in with their chosen email and password
- [ ] A user can toggle between the social or credentials sign in by clicking either "Sign in with username and password" or "Sign in with social"
- [ ] Clicking "Don't have an account, Sign up" or "Already have an account, Sign in" should take a user to the sign up page and credentials sign in page, respectively.
- [ ] When a user clicks "Sign in" on the Sign in page, the input should be validated to make sure it's not empty
- [ ] Providing a wrong username and password combination should present an error on the form, close to the offending form field. You can use your discretion here to create a good UI and UX since the design is not provided
- [ ] Validation error messages should show up as soon as the first characters are typed
- [ ] When a user clicks "Sign up" on the Sign up page, the input should be validated to ensure that
  - [ ] The email is a valid email address
  - [ ] The password is sufficiently strong (use your own discretion)
  - [ ] The first name is short and does not contain unexpected characters (e.g. links)
- [ ] Clear documentation or README.md provided, explaining how to set up and run the code
- [ ] Code should also be runnable using docker compose up
- [ ] Unit and/or integration test suite must be available and must execute by running `npm test` or `yarn test`

## Thanks!

If you made it this far, thanks for your time.
We look forward to reviewing your solid application with you!
